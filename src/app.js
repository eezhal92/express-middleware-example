const express = require('express');
const bodyParser = require('body-parser');

const db = require('./dummy-db');
const userDB = require('./user-db');
const jwt = require('jsonwebtoken');
const inputMiddleware = require('./middlewares/input');
const authMiddleware = require('./middlewares/auth');
const app = express();

// Agar bisa parse request body dengan type application/json
app.use(bodyParser.json());


// POST /login
// Implementasi sederhana, tidak di anjurkan
// Untuk naiveAuthGuardMiddleware
app.post(
  '/login', 
  inputMiddleware.makeInputGuard({
    email: 'required|email',
    password: 'required'
  }),
  // function (request, response, next) {
  //   const email = request.body.email;
  //   const password = request.body.password;

  //   if (!email) {
  //     return response.status(422).json({
  //       message: 'email dibutuhkan'
  //     })
  //   }

  //   if (!password) {
  //     return response.status(422).json({
  //       message: 'password dibutuhkan'
  //     })
  //   }

  //   next();
  // }, 
  function (request, response) {
    const email = request.body.email;
    const password = request.body.password;

    const user = userDB.findUserByEmailAndPassword(email, password);


    if (!user) {
      return response.status(404).json({
        message: 'Account not found'
      });
    }

    return response.json({
      token: user.token
    });
  }
);

// POST /login-b
// menggunakan jwt
app.post('/login-b', function (request, response) {
  const email = request.body.email;
  const password = request.body.password;

  const user = userDB.findUserByEmailAndPassword(email, password);

  if (!user) {
    return response.status(404).json({
      message: 'Account not found'
    });
  }

  const token = jwt.sign({ userId: user.id, email: user.email }, 'jwt-token');

  return response.json({
    token: token
  });
});

app.get(
  '/siapa-saya', 
  authMiddleware.jwtAuthGuard,
  function (request, response) {
    const data = request.userData;

    response.json({
      userId: data.userId,
      email: data.email
    })
  }
)

// POST vendors
app.post(
  // path
  '/vendors', 
  // middleware
  inputMiddleware.createVendorGuard,
  // request handler  untuk create vendor
  // handler ini mengharapkan ada field `title` dan `address` 
  // yang di sertakan dalam body dari request
  function (request, response) {
    const name = request.body.name;
    const address = request.body.address;

    const vendor = db.create(name, address);
    
    return response.json({
      vendor: vendor
    });
  }
);
// GET /vendors
app.get('/vendors', function (request, response) {
  return response.status(200).json({
    vendors: db.getAll(),
  });
});

app.get(
  '/protected', 
  authMiddleware.naiveAuthGuard, 
  function (request, response) {
    // Bila handler ini dieksekusi berarti request dikenal
    return response.json({
      message: 'Hey :)'
    });
});

// create user endpoint
// kita 'chain' beberapa middleware
app.post(
  '/users', 
  // pertama cek dulu apakah ter-otentikasi menggunakan naiveAuthGuard
  authMiddleware.naiveAuthGuard, 
  // kedua, apakah input valid atau tidak, kita 
  // panggil makeInputGuard agar hasil return
  // dari pemanggilan ini me-return function baru sebagai middeware
  // sehingga membuat input middleware menjadi flexible
  inputMiddleware.makeInputGuard({
    name: 'required|min:3',
    email: 'required|email',
    age: 'required|numeric'
  }),
  
  function (request, response) {
    const data = {
      name: request.body.name,
      email: request.body.email,
      age: request.body.age,
    }

    // todo insert to database
    response.status(201).json({
      message: 'Berhasil membuat user',
    });
  }
)

app.post(
  '/events', 
  authMiddleware.naiveAuthGuard, 
  // buat function middleware dengan rules yg berbeda
  inputMiddleware.makeInputGuard({
    title: 'required|min:3',
    date: 'required|datetime',
  }), 
  
  function (request, response) {
    response.status(201).json({
      message: 'Berhasil membuat event',
    });
  }
)

module.exports = app;