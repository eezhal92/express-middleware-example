const jwt = require('jsonwebtoken');

/**
 * @param {import("express").Request} request 
 * @param {import("express").Response} response 
 * @param {import("express").NextFunction} next 
 */
function naiveAuthGuard (request, response, next) {
  // Implementasi yang naive / bukan u/ production
  // Cuma untuk mengenal konsep
  // Middleware untuk mengecek apakah yang lagi bikin request dikenali atau tidak
  // Middleware ini mengharapkan request header bernama Authorization
  // value dari header tsb, mempunyai pola: `Bearer a-token`
  // bila `a-token` dikenali, maka request boleh dilanjutkan / dieksekusi
  // bila tidak, maka beri status code 401 Unauthorized
  
  // Implementasi auth middleware sederhana, kalo production biasanya pakai jsonwebtoken
  // @see https://github.com/auth0/node-jsonwebtoken
  
  // jika authorization header tidak ada, langsung kasih 401
  if (!request.header('authorization')) {
    return response.status(401).json({
      message: 'You are not authenticated',
    });
  }

  // Token yang dikenali, bila request menyertakan token ini
  // di request sperti ini:
  // Authorization: Bearer my-super-secret-token
  // atau
  // Authorization: Bearer my-second-known-token
  // Maka requestnya boleh diteruskan
  const knownTokens = [
    'my-super-secret-token',
    'my-second-known-token'
  ];
  
  // cek jika request menyertakan header Authorization
  // nilainya string semacam ini: Bearer a-token
  const authHeaderValue = request.header('authorization');
  // split jadi dua menggunakan method .split(), return valuenya adalah array
  // @see https://developer.mozilla.org/id/docs/Web/JavaScript/Reference/Global_Objects/String/split
  const splitted = authHeaderValue.split('Bearer '); 
  // nilai token ada di index 1
  const token = splitted[1];
  const isTokenValid = token && knownTokens.indexOf(token) > -1;

  // jika token tidak valid, maka beri status 401
  if (!isTokenValid) {
    return response.status(401).json({
      message: 'You are not authenticated',
    });
  }

  // otherwise, lanjutkan ke next middleware / request handler
  return next();
}

function jwtAuthGuard(request, response, next) {
  if (!request.header('authorization')) {
    return response.status(401).json({
      message: 'You are not authenticated',
    });
  }

  const authHeaderValue = request.header('authorization');
  const splitted = authHeaderValue.split('Bearer '); 
  const token = splitted[1];

  let decoded = null;
  try {
    decoded = jwt.decode(token, 'jwt-token');
  } catch (error) {
    return response.status(400).json({
      message: error.message,
    });
  }

  request.userData = decoded;

  next();
}

module.exports = {
  naiveAuthGuard: naiveAuthGuard,

  jwtAuthGuard: jwtAuthGuard
}