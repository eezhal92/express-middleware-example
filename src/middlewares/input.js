const Validator = require('validatorjs');

/**
 * Middleware u/ validasi input untuk membuat vendor
 * @param {*} request 
 * @param {*} response 
 * @param {*} next 
 */
function createVendorGuard(request, response, next) {
  const input = request.body;

  const errors = [
    
  ];
  
  if (!input.name) {
    errors.push({ name: 'name is required' });
  };

  if (!input.address) {
    errors.push({ address: 'address is required' });
  };

  const hasError = Boolean(errors.length);
  if (hasError) {
    // status code 422 menandakan input user
    // tidak valid
    return response.status(422).json({
      errors: errors,
    });
  }

  // Jika tidak ada error, maka eksekusi middleware selanjutnya (jika ada)
  // kemudian eksekusi request handler
  return next();
}

/**
 * Fungsi untuk membuat middleware function secara dinamis
 * @see https://github.com/skaterdav85/validatorjs
 * @param {object} rules  contoh: { title: 'required|min:3', price: 'number' }
 * @return {Function} menghasilkan middleware function untuk validasi input
 */
function makeInputGuard(rules) {
  return function inputGuard(request, response) {
    const data = request.body;

    const validation = new Validator(data, rules);

    // Jika input tidak memenuhi syarat
    if (validation.fails()) {
      return response.status(422).json({
        errors: validation.errors,
      });
    }

    // lanjut ke middleware berikutnya atau ke request handler
    next();
  }
}

module.exports = {
  makeInputGuard,
  createVendorGuard,
}