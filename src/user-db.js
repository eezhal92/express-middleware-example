const users = [
  { id: 1, email:'johndoe@gmail.com', password: 'password', token: 'my-super-secret-token' },
  { id: 2, email:'alex@gmail.com', password: 'rahasia', token: 'my-second-known-token' }
];

function findUserByEmailAndPassword(email, password) {
  return users.find(user => user.email === email && user.password === password);
}

module.exports = {
  findUserByEmailAndPassword,
}