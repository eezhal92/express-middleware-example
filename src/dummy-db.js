/**
 * Implementasi in-memory database sederhana
 */

const vendors = [
  { id: '1', name: 'Abc', address: 'Ave 4th' },
  { id: '2', name: 'Def', address: 'Brooklyn 123' },
];

/**
 * Get all vendors data.
 */
function getAll() {
  return vendors;
}

/**
 * Create new vendor data.
 * @param {string} name 
 * @param {string} address 
 */
function create(name, address) {
  const vendor = {
    id: Math.random(),
    name: name,
    address,
  };

  vendors.push(vendor);

  return vendor;
}

module.exports = {
  getAll: getAll,

  // ES6 Short-hand / Sama -> create: create
  create,
}